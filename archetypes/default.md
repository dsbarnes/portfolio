---
title: {{ replace .File.TranslationBaseName "-" " " | title }}
subtitle:
date: {{ .Date }}
description: Twitter Card and OG Text

draft: true
robotsdisallow: false

categories: []
tags: []
series: []

video: 
audio: 

image: 
imgalt:
figcaption:
---