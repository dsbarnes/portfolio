---
title: LetterClef
subtitle:
date: 2022-02-24T16:54:04-05:00
description: Twitter Card and OG Text

draft: false
robotsdisallow: false

categories: []
tags: []
series: []

video: 
audio: 

image: 
imgalt:
figcaption:
---

# LetterClef

<!-- Hero -->
<img class="hero-image" src="/img/LetterClef/Large-Pink.png">

<!-- Client Background -->
<section class="project-section">
    <div>
        <strong>Client Background</strong>

Eitan Schider is … well… was a traveling guitarist. He regularly toured the southern and midwest regions of the US, playing in any blues jam or jazz session that would have him.

Covid lockdowns ended that. Nearly overnight.

Eitan did all he could, but playing regular gigs in shady holes that refused to close certainly didn’t make the money tours did. The audiences were as thin as he could remember, and whenever an open mic opportunity was available, the tip jar remained empty.

One night Eitan had finished his few minutes at the mic, went out for some air and saw a man reach into his pocket and pull out his phone. When he did about a dollar or so of change fell to the ground. The, young, intoxicated man looked down, and waved if off.  Eitan looked down at his empty metal tip bucked and thought to himself, “I could make more in tips by picking change from the parking lot or off the bar room floor than I can playing for these crowds. Something has to change.”

Never one to quit, that very night he went home and began looking into how to publish books, videos, and engaging on social media. Over the next months he’d completed Google certifications in marketing and SEO. Soon after he began learning how to code and manage websites.

Quickly Eitan realized being a musician, audio engineer, video editor, digital marketing manager, copywriter, web developer, sales lead, and secretary was … impossible. There’s only so many hours in a day.

About this time, my phone rang.
    </div>
    <img class="side-image" src="/img/LetterClef/Small-Light-Blue.png">
</section>


<!-- Project Summary -->
<section class="project-section">
    <div>
        <strong>Project Summary</strong>

Eitan, through trial and error, discovered he really didn’t like making videos. He knew video was very important to his overall social media strategy, but ideating, storyboarding, the process of recording, and then editing all of it, was obviously too much for one person. However, writing, and recording music was exactly his lane. He could produce one example after another in few takes. Furthermore, he had a decent sized collection of very well reviewed music instruction books, which he loved to re-read.

About this point, it was clear to me what I needed to do.

Static site generators were designed with people like Eitan in mind.

His site consists of three layouts, the core of the book, the advanced lessons readers can purchase, and pages for the advanced lessons. SEO is a high priority, therefore we want to avoid site generators that spit out poor hierarchy, like WordPress.
    </div>
    <img class="side-image" src="/img/LetterClef/Small-Pink.png">
</section>


<!-- Challenge -->
<section class="project-section">
    <div>
        <strong>Challenge</strong>

Eitan is a musician, and a writer. He knows that world very well.
What he needs is a very simple interface to update his website. He is willing to be trained to manage updates and deploys himself, so long as the process is clear, quick, and simple. We need to go all in on SEO from the beginning. Eitan has time to dedicate to social media, but only a small percentage of his day. As the sites readership grows Eitan will be able to create a funnel to his YouTube channel, to gain one thousand subscribers, and begin live streaming.

All this is fine and good, but Eitans been out of work for over 6 months, and needs to generate any amount of income he can ASAP. Therefore, we’ve separated the site into the core book, and advanced lessons that are for sale at a very reasonable price.    
    </div>
    <img class="side-image" src="/img/LetterClef/Small-Purple.png">
</section>


<!-- Solution -->
<section class="project-section">
    <div>
        <strong>Solution</strong>

Eitan is now the proud owner of LetterClef.com

His website is his book, all on one static page as a matter of SEO strategy, with a table of contents fixed in the sidebar. Users can read the core material for free. Along the way are calls to action to join the mailing list, and follow his social media channels, and support his work through donations. All images on the pages are SVG for performances and scale. 

Users can navigate to the advanced lesson section where they can purchase PDF copies of his advanced lessons. A members section contains all the purchases a user has made. Both layouts contain the CTAs for the mailing list, social media, and donations.

Eitans site is as accessible and performant as websites come, and the SEO metrics are immaculate.
    </div>
    <img class="side-image" src="/img/LetterClef/Small-Green.png">
</section>

<!-- Image Set -->
<section class="bottom-image-section">
    <img class="bottom-image" src="/img/LetterClef/Large-Half-Light-Blue.png">
    <img class="bottom-image" src="/img/LetterClef/Large-Half-Purple.png">
</section>
<section class="bottom-image-section">
    <img class="bottom-image" src="/img/LetterClef/Large-Half-Green.png">
    <img class="bottom-image" src="/img/LetterClef/Large-Half-Pink.png">
</section>