---
title: TheGraphicArt
subtitle:
date: 2022-02-24T16:54:14-05:00
description: Twitter Card and OG Text

draft: false
robotsdisallow: false

categories: []
tags: []
series: []

video: 
audio: 

image: 
imgalt:
figcaption:
---

# TheGraphic.Art

<!-- Hero -->
<img class="hero-image" src="/img/TheGraphicArt/Large-Purple.png">

<!-- Client Background -->
<section class="project-section">
    <div>
        <strong>Client Background</strong>

Gosman, Ben, and Joseph met in their college philosophy club and quickly became inseparable friends. They were able to overlap their class projects for nearly three years. Gosman was a top notch researcher from the psychology department, Joseph a walking textbook on art history and a graphic designer, and Ben a prolific writer of the highest caliber. Together they designed event posters for local bars and bands, managed interior design projects for new local business, and were all invited to be editors at Stance magazine, the largest undergraduate philosophy journal in the United States.

After a successful year at Stance it was time for graduation. As is life for many, it was their turn to move off and develop professionally. For several years each successfully lived the life they’d dreamed of in college. 

Some five-ish years later they make a discovery about one another. Josephs was in the process of moving back to Indiana, and Gosmans was moving closer to his office thanks to a recent promotion. Those two moves placed them within three miles of one another.

Their friendship unfazed by distance or time, the philosophy club had no choice in reuniting. Greater forces had made that decision long ago. Of course, I wouldn’t be writing this if they’d only gotten together to have some drinks every weekend. They needed another project. One that was lasting, real, and fulfilling to each of their passions. One that could be done remotely. One that would continue and spark interest in the arts and philosophy of the readers.

That’s when my phone rang.
I was in the techie of the philosophy club.
    </div>
    <img class="side-image" src="/img/TheGraphicArt/Small-Light-Blue.png">
</section>


<!-- Project Summary -->
<section class="project-section">
    <div>
        <strong>Project Summary</strong>

We wanted to build our own Stance magazine.

TheGraphic.Art

A web based zine on a mission to promote and provide educational studies, and learn-by-doing style projects for the college students we used to be.
    </div>
    <img class="side-image" src="/img/TheGraphicArt/Small-Pink.png">
</section>


<!-- Challenge -->
<section class="project-section">
    <div>
        <strong>Challenge</strong>

Our vision was a simple static site, with no frills, bells, whistles, or anything even near the kitchen sink. We wanted to be as close to text and simple images as we could.

Obviously, sharing our work was important. The open graph and Twitter card meta game needed to be on point, as well other aspects of the SEO.

Knowing the images were coming from various sources (all CC0 works) we would need some type of logic to serve the correct image sizes. That’s what image CDNs are for.

Finally, as this is our hobby and we don’t expect to see money returned to us for a very long time, if at all, we needed the scaling to be a simple and free as possible. 
    </div>
    <img class="side-image" src="/img/TheGraphicArt/Small-Dark-Blue.png">
</section>


<!-- Solution -->
<section class="project-section">
    <div>
        <strong>Solution</strong>

Hugo, ImageKit, and Netlify were designed to do exactly what we want.
Other than that, I decided on fonts and we were good to develop a frame for posting articles.

Currently, I write articles, handle updates and deploy of the site, as well as managing the CDN.
    </div>
    <img class="side-image" src="/img/TheGraphicArt/Small-Green.png">
</section>

<!-- Image Set -->
<img class="bottom-image" src="/img/TheGraphicArt/Large-Green.png">
<img class="bottom-image" src="/img/TheGraphicArt/Large-Pink.png">