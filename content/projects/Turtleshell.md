---
title: Turtleshell
subtitle:
date: 2022-02-24T16:54:32-05:00
description: Twitter Card and OG Text

draft: false
robotsdisallow: false

categories: []
tags: []
series: []

video: 
audio: 

image: 
imgalt:
figcaption:
---

# Turtleshell

<!-- Hero -->
<img class="small-hero-image" src="/img/Turtleshell/Large-Half-Green.png">
<img class="small-hero-image" src="/img/Turtleshell/Large-Half-Purple.png">
<img class="small-hero-image" src="/img/Turtleshell/Large-Half-Dark-Blue.png">
<img class="small-hero-image" src="/img/Turtleshell/Large-Half-Pink.png">

<!-- Client Background -->
<section class="project-section">
    <div>
        <strong>Client Background</strong>

This is a personal project that I decided to do after a reading the Vignelli Canon.

He writes: “I made an exhibition showing work that we had done over many years by using only four typefaces: Garamond, Bodoni, Century Expanded, and Helvetica. The aim of the exhibition was to show that a large variety of printed matter could be done with an economy of type with great results. In other words, is not the type but what you do with it that counts.“

I feel about the same as Vignelli. 
    </div>
    <img class="side-image" src="/img/Turtleshell/Small-Light-Blue.png">
</section>


<!-- Project Summary -->
<section class="project-section">
    <div>
        <strong>Project Summary</strong>

Color blindness can range from rather small differences in perceived color to monochromicy (ones whole world is grayscale). When I cam up as a developer, accessibility was always a priority. For one, we want people to be able to use our web product, and two, it helps SEO.

However, I’m on websites every day that either don’t consider accessibility, or don’t care.

Because I consider accessibility, think about it often, and care, I decided to create a design system that I could use as reference for creating and sourcing extraordinary accessible color pallets.
    </div>
    <img class="side-image" src="/img/Turtleshell/Small-Pink.png">
</section>


<!-- Challenge -->
<section class="project-section">
    <div>
        <strong>Challenge</strong>

Minimalism is something I’m fascinated with. It almost feels like the end goal of design. Vignelli and I would say it works for Typograph, but I wanted to veer into Lucian Bernhard territory and work with a color palette.

Just how many colors could I fit into a pallet that looked as similar as possible across all spectrums of colorblindness? My goal was to discover a method for creating universally accessible color schemes for the 7 types of colorblindness, as well as avoid colors known to aggravate photophobia.
    </div>
    <img class="side-image" src="/img/Turtleshell/Small-Dark-Blue.png">
</section>


<!-- Solution -->
<section class="project-section">
    <div>
        <strong>Solution</strong>

I did it!

Turtleshell utilizes a 5 color system, each unique and coherent across all 7 types of color blindness. Many of the most common components were created, with variation for two categories of statefulness, the message state (info, success, warning, error) and the action state, (enabled, hoover, focus, selected, pressed, active, and disabled).

Turtleshell is available free at Figma.community.whatever.

Updates are forever rolling, but the system is well passed an MVP and ready use now.
    </div>
    <img class="side-image" src="/img/Turtleshell/Small-Purple.png">
</section>

<!-- Image Set -->
<img class="bottom-image" src="/img/Turtleshell/Large-Green.png">