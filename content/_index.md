# Greetings, I'm Derek.<br>I Design Brand Identities
Logos, Icons, Cards, Letterhead, Labeling, Advertising, Social Content, & Websites

<section id="front-page-projects">
    <div class="front-page-card">
        <a href="/projects/thegraphicart">
        <img src="/img/TheGraphicArt/TheGraphicArt-Home.png">
        <p></p>
        <p>
            <strong>TheGraphic.Art</strong>
            <br>
            <br>
            A music theory book focused on theory, blues, and jazz that became a website
            <br>
            <br>
            Website // Logo // Graphics
        <p>
        </a>
    </div>
    <div class="front-page-card"> 
        <a href="/projects/letterclef">
        <img src="/img/LetterClef/LetterClef-Home.png">
        <p></p>
        <p>
            <strong>LetterClef</strong>
            <br>
            <br>
            A college publication on a mission to expound on modern design history.
            <br>
            <br>
            Website // Logo // Graphics
        <p>
    </div>
    <div class="front-page-card"> 
        <a href="/projects/turtleshell">
        <img src="/img/Turtleshell/Turtleshell-Home.png">
        <p></p>
        <p>
            <strong>Turtleshell</strong>
            <br>
            <br>
            A design system for creating colo blind friendly user interfaces.
            <br>
            <br>
            Project Owner
        <p>
        <a>
    </div>
</section>